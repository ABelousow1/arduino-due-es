// SKETCH_NAME: au2_template.ino
// ##### Aufgabe 2.x #####
// analog input/output

#define pin_led 13	//externe LED
#define pin_bu  3	//button pin
#define pin_poti 9 //analog input poti

int brightness = 1;
int brightness_before = 0;

void setup()
{
  pinMode(pin_bu, INPUT);
  pinMode(pin_led, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  brightness = analogRead(pin_poti);
  if (abs(brightness_before - brightness) > 3){
      Serial.println(brightness);
      Serial.print("Spannung: ");
      Serial.println(3.3f/1023 * brightness);
      brightness_before = brightness;
   }
   analogWrite(pin_led, brightness/1023.0f * 255);
 
}
