// ##### Aufgabe 0.0 #####
// Digital I/O / Blinking LED
// Board connection pin of the LED
// ANODE (+) -> "longer leg" of the LED

uint8_t pin_led = 13;  //Nr. des ext. Pins
bool ledState = LOW;   //Zustandsvariable

uint8_t pin_button = 3;

bool prev_buttonstate = LOW;

bool buttonstate;

// ################################################################################


void toggle_ledstate() {
  ledState = !ledState;
}

void turn_led_with_button() {
  buttonstate = digitalRead(pin_button);
  if (!buttonstate) {
    if (prev_buttonstate) {
      toggle_ledstate();
    }
  }
  prev_buttonstate = buttonstate;
}

void setup() {
  pinMode(pin_led, OUTPUT);
  pinMode(pin_button, INPUT);
  digitalWrite(pin_led, ledState);
  buttonstate = digitalRead(pin_button);
}

void loop() {

  turn_led_with_button();

  

  digitalWrite(pin_led, ledState);
}