#define INPUT_LENGTH 127
uint8_t pin_led = 13;
uint8_t pin_poti = 9;
uint8_t pin_button = 3;
char input_buffer[INPUT_LENGTH] = { 0 };
uint8_t buffer_pos = 0;
int led_state = 0;

int button_state = HIGH;
int button_value_before = LOW;

int brightness = 0;
int brightness_before = 0;

char help[] = "Entwickeln Sie einen String-Parser,\n\
welcher ein über das Eingabefeld des seriellen Monitors abgeschicktes Kommando entgegennimmt\n\
 und dieses auf Korrektheit überprüft.\n\
  Bei einem korrekt erkannten Kommando veranlassen Sie die Ausführung der Funktion, andernfalls geben Sie,\
soweit möglich, eine möglichst aussagekräftige Fehlermeldung auf der seriellen Konsole aus.\n\
Der Befehlssatz soll folgende Kommandos enthalten:\n\
• help()\n\
Listet die gültigen Befehle mit Angabe von Information zur Nutzung der Befehle in der\n\
Ausgabe des seriellen Monitors auf.\
• LEDon()\n\
schaltet die LED mit dem voreingestellten Helligkeitswert ein.\n\
• LEDoff()\n\
schaltet die LED aus\n\
• illuminance(value)\n\
Lichtintensität (value) der LED in [%]\n";
void print_help() {
  Serial.println(help);
}

void illuminance(int brightness) {
  led_state = brightness;
}
const char HELP_STR[] = "help()";
const char LEDON_STR[] = "LEDon()";
const char LEDOFF_STR[] = "LEDoff()";
const char ILLUMINANCE_BEFORE_STR[] = "illuminance(";
const char ILLUMINANCE_AFTER_STR[] = ")";

bool compare_first_part_with_input(const char* b, const int size_b) {
  for (int i = 0; i < size_b; ++i) {
    if (input_buffer[i] != b[i]) return false;
  }
  return true;
}
void do_command(char* command, int size) {
  if (compare_first_part_with_input(HELP_STR, sizeof(HELP_STR) - 1)) {
    print_help();
  } else if (compare_first_part_with_input(LEDON_STR, sizeof(LEDON_STR) - 1)) {
    illuminance(255);
  } else if (compare_first_part_with_input(LEDOFF_STR, sizeof(LEDOFF_STR) - 1)) {
    illuminance(0);
  } else if (compare_first_part_with_input(ILLUMINANCE_BEFORE_STR, sizeof(ILLUMINANCE_BEFORE_STR) - 1) && input_buffer[strlen(input_buffer) - 1] == ')') {
    int n = atoi(input_buffer + sizeof(ILLUMINANCE_BEFORE_STR) - 1);
    illuminance(n);
  } else {
    Serial.println("Erron. Commando is undefined.");
  }
}
void set_brightness_from_poti() {
  int value = analogRead(pin_poti);
  if (abs(brightness_before - value) > 3) {
    brightness = value;
    Serial.println(brightness);
    Serial.print("Spannung: ");
    Serial.println(3.3f / 1023 * value);
    brightness_before = value;
  }
}
void toggle_buttonstate() {
  button_state = !button_state;
}
void set_button_state() {
  int button_value = digitalRead(pin_button);
  if (!button_value) {
    if (button_value_before) {
      toggle_buttonstate();
    }
  }
}
void setup() {
  Serial.begin(9600);
  Serial.print("your input: ");
  pinMode(pin_button, INPUT);
  pinMode(pin_led, OUTPUT);
}

void loop() {
  while (Serial.available()) {
    char ch = Serial.read();  //read char
    Serial.write(ch);         //echo to serial monitor
    if ((ch != 0x0A) && (ch != 0x0B) && (ch != 0x0C) && (ch != 0x0D)) {
      input_buffer[buffer_pos++] = ch;
      if (buffer_pos == INPUT_LENGTH) {
        buffer_pos = 0;
        Serial.println("WARNING: Input buffer overflow!");
      }
    } else {  // Output input string

      do_command(input_buffer, sizeof(input_buffer));

      input_buffer[buffer_pos] = '\0';
      Serial.print("read in line: ");
      Serial.println(input_buffer);
      buffer_pos = 0;
      input_buffer[buffer_pos] = '\0';
    }
  }
  set_button_state();
  set_brightness_from_poti();
  if (button_state == HIGH) {
    led_state = brightness;
  } else {
    led_state = 0;
  }
  analogWrite(pin_led, led_state);
}